import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/Homepage.vue'

describe('Homepage.vue', () => {

  it('renders props.msg when passed', () => {
    const msg = 'Lukas Audzevicius'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })

  it('renders props.msg2 when passed', () => {
      const msg2 = 'Software developer,'
      const wrapper2 = shallowMount(HelloWorld, {
          propsData: { msg2 }
      })
      expect(wrapper2.text()).toMatch(msg2)
  })

  it('renders props.msg3 when passed', () => {
    const msg3 = 'sometimes human.'
    const wrapper3 = shallowMount(HelloWorld, {
        propsData: { msg3 }
    })
    expect(wrapper3.text()).toMatch(msg3)
})
})