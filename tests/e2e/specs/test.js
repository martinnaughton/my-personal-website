// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'Test website is live': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#app', 2500)
      .end()
  },
  'Test navbar exists': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#nav', 2500)
      .end()
  },
  'Test navbar redirects to other pages': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('#nav', 2500)
      .click('#nav-link-1')
      .assert.urlEquals('http://localhost:8080/Projects')
      .waitForElementVisible('#nav', 2000)
      .click('#nav-link-2')
      .assert.urlEquals('http://localhost:8080/Contact')
      .waitForElementVisible('#nav', 2000)
      .click('#nav-link-0')
      .assert.urlEquals('http://localhost:8080/')
      .waitForElementVisible('#nav', 2000)
      .end()
  },
  'Test projects page loads up': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#nav', 2500)
    .click('#nav-link-1')
    .assert.urlEquals('http://localhost:8080/Projects')
    .waitForElementVisible('#nav', 2000)
    .assert.containsText('h1', 'My projects')
    .end()
  },
  'Test contact page loads up': browser => {
    browser
    .url(process.env.VUE_DEV_SERVER_URL)
    .waitForElementVisible('#nav', 2500)
    .click('#nav-link-2')
    .assert.urlEquals('http://localhost:8080/Contact')
    .waitForElementVisible('#nav', 2000)
    .assert.containsText('h1', 'Do you want to contact me?')
    .end()
  }
}
